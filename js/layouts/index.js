/* global define, Marionette */
define([
    'text!templates/layouts/index.html'
], function (indexTemplate) {
    var IndexLayout = Marionette.Layout.extend({
        template: indexTemplate,

        regions: {
            statusRegion: ".statusRegion"
        }
    });
    return IndexLayout;
});