/* global define, sch */
define(['config/global'], function(){
    sch.config.apiUrl = "http://www.mocky.io/v2/";

    return sch.config;
});
