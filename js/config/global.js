/* global define, sch */
define(function () {
    window.sch = window.sch || {};
    sch.config = sch.config || {};

    sch.config.requestTryAgainTime = 2000;

    return sch.config;
});


