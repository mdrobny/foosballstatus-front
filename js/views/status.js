/* global define, sch, Marionette */
define([
    'models/status',
    'text!templates/status.html'
], function (StatusModel, statusTemplate) {
    var statusView = Marionette.Layout.extend({
        template: statusTemplate,
        model: null,

        initialize: function() {
            this.model = new StatusModel();

            this.getData();
            setInterval(function(){
                this.getData();
            }.bind(this), 10000);
        },

        getData: function() {
            $.when(this.fetchData())
                .then(function () {
                    this.render();
                }.bind(this))
                .fail(function ()  {
//                    setTimeout(this.getData.bind(this), sch.config.requestTryAgainTime);
                }.bind(this));
        },

        fetchData: function() {
            return this.model.fetch();
        }



    });
    return statusView;
});