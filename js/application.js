/* global define, Backbone, sch, Marionette */
define([
    'router',
    'layouts/index'
], function(Router, IndexLayout){
    var app, indexLayout;

    Marionette.TemplateCache.prototype.loadTemplate = function(template) {
        return template;
    };

    app = new Marionette.Application();

    app.addRegions({
        indexRegion: ".indexRegion"
    });

    app.addInitializer(function () {


        this.router = new Router();

        Backbone.history.start();
    });

    indexLayout = new IndexLayout();
    app.layout = indexLayout;
    app.indexRegion.show(indexLayout);

    sch.app = app;

    return sch.app;
});