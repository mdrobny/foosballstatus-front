/*global define, sch */
define([], function(){
    window.sch = window.sch || {};

    sch.string = {
        /**
         * Changes string to proper function name
         *
         * @param str
         * @returns {*|void}
         */
        toFunctionName: function(str) {
            return str.replace(/\W/g, '_');
        },

        /**
         * Format number with leading 0 if lower than 10
         *
         * @param number
         * @returns {string}
         */
        zeroPad: function(number) {
            return number < 10 ? '0' + number : number;
        },

        /**
         * Change unsafe characters from given string to html codes
         *
         * @url http://lodash.com/docs#escape
         * @param str
         * @returns {*|void}
         */
        escape: _.escape,

        /**
         * Change unsafe characters from given string to html codes
         *
         * @url http://lodash.com/docs#unescape
         * @param str
         * @returns {*|void}
         */
        unescape: _.unescape,

        /**
         * Remove all unsafe html characters
         */
        stripTags: function(text) {
            return text.replace(/(<([^>]+)>)/ig,"");
        }

        /**
         * Slug url parameters
         * Cuts off
         */
//        slug: Axis.Router.prototype.slugParam
    };

    return sch.string;
});
