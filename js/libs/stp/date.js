/*global define, sch */
define([
    'libs/stp/string'
], function(){
    window.sch = window.sch || {};
    /**
     * Class for manipulating strings
     * @type {{zeroPad: *, format: Function}}
     */
    sch.date = {
        zeroPad: sch.string.zeroPad,

        /**
         * converts date to proper string
         * corrects month's number (+1)
         * @param date
         * @returns {string}
         */
//        dateTimeToString: function(date) {
//            return date.getFullYear() +'-'+ this.zeroPad(date.getMonth() + 1) +'-'+ this.zeroPad(date.getDate()) +' '+
//                this.zeroPad(date.getHours()) +':'+ this.zeroPad(date.getMinutes()) +':'+ this.zeroPad(date.getSeconds());
//        },
        dateToString: function(date) {
            return date.getFullYear() +'-'+ this.zeroPad(date.getMonth() + 1) +'-'+ this.zeroPad(date.getDate());
        },
        /**
         * returns days amount in month
         * corrects month's number (+1)
         * @param month
         * @param year
         * @returns int
         */
//        daysInMonth: function(month, year) {
//            var date = new Date(year, ++month, 0);
//            return date.getDate();
//        },
        /**
         * Get days between two dates
         * @param date1
         * @param date2
         * @returns {*}
         */
        daysBetweenDates: function(date1, date2) {
            var timeDiff = Math.abs(date2.getTime() - date1.getTime()),
                diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            return diffDays;
        }
        /**
         * Get hours amount between two dates
         * @param date1
         * @param date2
         * @returns {*}
         */
//        hoursBetweenDates: function(date1, date2) {
//            var timeDiff = Math.abs(date2.getTime() - date1.getTime()),
//                diffHours = Math.ceil(timeDiff / (1000 * 3600));
//            return diffHours;
//        }
    };

    return sch.date;
});
