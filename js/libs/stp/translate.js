/*global define, sch:true */
define(['data/lang'], function(_dict) {
    window.sch = window.sch || {};

    sch.translate = function(key) {
        return _dict[key] || '';
    };

    return sch.translate;
});
