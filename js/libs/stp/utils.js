/*global define, sch */
define([
    'libs/stp/string',
    'libs/stp/date'
], function(){
    sch.utils = {};

    sch.utils.exportToCSV = function(chart, fileName) {
        var data = chart.getCSV();
        sch.utils.saveDataToFile(data, fileName);
    };

    sch.utils.saveDataToFile = function(data, fileNameArg) {
        var blob, fileName, downloadElem;

        blob = new Blob([data], {type: 'text/csv'});
        fileName = fileNameArg ||  "notifave-data.csv";

        downloadElem = document.createElement("a");
        downloadElem.download = fileName;
        downloadElem.innerHTML = "Download CSV";
        if (window.webkitURL !== undefined) {
            /* Chrome allows the link to be clicked
             * without actually adding it to the DOM.
             * */
             downloadElem.href = window.webkitURL.createObjectURL(blob);
        } else {
            /* Firefox requires the link to be added to the DOM
             * before it can be clicked.
             */
            downloadElem.href = window.URL.createObjectURL(blob);
            downloadElem.onclick = function(event) {
                document.body.removeChild(event.target);
            };
            downloadElem.style.display = "none";
            document.body.appendChild(downloadElem);
        }

        downloadElem.click();
    };

    /**
     * sets exporting buttons
     * dependency: this.chart used in callback of item onClick
     * @param exportedFileName: name of file to download
     * @returns {{contextButton: {menuItems: Array}}}
     */
    sch.utils.chartImgExportButtons = function(exportedFileName) {
        var now, buttons;
        now = new Date();
        exportedFileName += "_"+ sch.date.dateToString(now);

        buttons = function() {
            return {
                contextButton: {
                    menuItems: [{
                        text: 'Save as PNG',
                        onclick: function() {
                            this.chart.exportChart({
                                type: 'image/png',
                                filename: exportedFileName
                            });
                        }.bind(this)
                    }, {
                        text: 'Save as JPEG',
                        onclick: function() {
                            this.chart.exportChart({
                                type: 'image/jpeg',
                                filename: exportedFileName
                            });
                        }.bind(this)
                    }]
                }
            };
        }.bind(this);
        return buttons();
    };

    return sch.utils;
});
