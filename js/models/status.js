/* global define, Backbone, sch */
define(function  () {
    var status = Backbone.Model.extend({
        defaults :  {
            status: "",
            lastStatusChange: "",
            movement: {
                startDate: "",
                photo: "",
                scores: {
                    red: 0,
                    blue: 0
                }
            },
            calendar: {
                title: "",
                eventOwner: "",
                startData: "",
                finishDate: ""
            }
        },

        url: function() {
//            return sch.config.apiUrl +"api/corner/status";
//            return sch.config.apiUrl +"52a97c06e2eab53e068a0ce4";   //free
//            return sch.config.apiUrl +"52a97cd8e2eab555068a0ce5";   //occupied
//            return sch.config.apiUrl +"52a97ceee2eab555068a0ce6";   //gameOn
            return "http://192.168.10.246/foosballstatus/api/corner/status";   //gameOn
        }
    });
    return status;
});