/*global require, define */
require.config({
    enforceDefine: true,
    paths: {
        text: 'libs/requirejs-plugins/text',
        "app-config": 'config/local',
        templates: '../templates'
    },
    urlArgs: "bust=" +  (new Date()).getTime()
});

define([
    'app-config',
    'libs/stp/utils'
], function () {
    require(['application'], function(app) {
        app.start();
    });
});
