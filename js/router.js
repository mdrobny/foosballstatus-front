/* global define, Backbone, sch */
define([
    'views/status'
], function(statusView) {
    var Router = Backbone.Router.extend({

        routes: {
            "*actions" : "defaultRoute"
        },

        defaultRoute: function() {
            sch.app.layout.statusRegion.show(new statusView());
        }
    });

    return Router;
});
